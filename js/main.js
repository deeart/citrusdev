jQuery(document).ready(function($){
	
	// unscroll func
	function freeze() {
        if($("html").css("position") != "fixed") {
            var top = $("html").scrollTop() ? $("html").scrollTop() : $("body").scrollTop();
            if(window.innerWidth > $("html").width()) {
                $("html").css("overflow-y", "scroll");
            }
            $("html").css({"width": "100%", "height": "100%", "position": "fixed", "top": -top});
        }
    }
    function unfreeze() {
            if($("html").css("position") == "fixed") {
                $("html").css("position", "static");
                $("html, body").scrollTop(-parseInt($("html").css("top")));
                $("html").css({"position": "", "width": "", "height": "", "top": "", "overflow-y": ""});
            }
    }
    var frozen = false;
	
	
	//open-close menu numbers on mobile
	$('.call_nums').on('click', function(event){
		$(this).children('ul').on('click', function(event){
				return false;
		});
		if($(event.target).is('.call_nums')) {
			$(this).toggleClass('opened');
			$(".cd-header").toggleClass('open-nav');
		}
		var bg_blur = "#blur";
		if ($(bg_blur).length){ 
			$(bg_blur).blurbox().hide();
			$(bg_blur).remove();
		}
		else {
			$('body').append('<div id="blur"></div>');
			$(bg_blur).blurbox().show();
		}
		
		if(frozen) {
            unfreeze();
            frozen = false;
        } else {
            freeze();
            frozen = true;
        }
	});
	$('#blurbox-darkenbg').on('click', function(){
			var bg_blur = "#blur";
			$('.call_nums').toggleClass('opened');
			$(".cd-header").toggleClass('open-nav');
			$(bg_blur).blurbox().hide();
			$(bg_blur).remove();
			
		if(frozen) {
            unfreeze();
            frozen = false;
        } else {
            freeze();
            frozen = true;
        }
	});
	
	
	
	//open-close main menu on mobile
	$('.cd-main-nav').on('click', function(event){
		if( $(window).width() < 992){
			$(this).children('ul').on('click', function(event){
					return false;
			});
			if($(event.target).is('.cd-main-nav')) {
				$(this).toggleClass('is-open');
				$(".cd-header").toggleClass('open-nav');
				$(this).children('ul').toggleClass('is-visible');
			}
			var mbg = "#mbg";
			if ($(mbg).length){ 
				$(mbg).fadeOut( 300, function() {
					$(mbg).remove();
				});
			}
			else {
				$('body').append('<div id="mbg"></div>'); 
				$(mbg).fadeIn(300);
			}

			if(frozen) {
				unfreeze();
				frozen = false;
			} else {
				freeze();
				frozen = true;
			}
		}
	});
	
	//open-close slide main menu on mobile
	$('.cd-main-nav > ul > li').on('click',  function(event){
		if( $(window).width() < 992){
			if (!$(this).hasClass('is-active')) {
					event.preventDefault();
				$('.cd-main-nav > ul > li').removeClass('is-active');
				$('.cd-main-nav > ul > li').children('ul').slideUp();

				$(this).children('ul').slideDown();
				$(this).addClass('is-active');

			} else {
					event.preventDefault();
					$(this).children('ul').slideUp();
					$(this).removeClass('is-active');
			}
		}
	});
	
	
	// head nav scroll
	if (!$('body').hasClass('home')) { $(".cd-header").addClass("scrolled"); }
	$(window).scroll(function() {
		if ($('body').hasClass('home')) {
		  	var scroll = $(window).scrollTop();
			if (scroll >= 105) {
				$(".cd-header").addClass("scrolled");
			} else {
				$(".cd-header").removeClass("scrolled");	
			}
		}	
	});
	
	
	// contact index maps
	function region_zoom(region){
		var r_zoom = {"us": 12, "ca": 12, "is": 8, "ua": 7 };
		return r_zoom[region];
	}
	function init_map(c_x,c_y,c_name,c_zoom){var myOptions = {zoom:c_zoom,center:new google.maps.LatLng(c_x,c_y),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(c_x,c_y)});infowindow = new google.maps.InfoWindow({content: c_name });google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}
	
	var first_map_tab = "#map-tabs li:first";
	$(first_map_tab).addClass("active");
		var c_region = $(first_map_tab).attr("data-region");
		var c_zoom = region_zoom(c_region);
		var c_x = $(first_map_tab).attr("data-map-x");
		var c_y = $(first_map_tab).attr("data-map-y");
		var c_name = $(first_map_tab).attr("data-title");
		$('#region-'+c_region).addClass("active");
		
		google.maps.event.addDomListener(window, 'load', init_map(c_x,c_y,c_name,c_zoom));
	
	$("#map-tabs li").click(function(){
		$(".active").removeClass("active");
		$(this).addClass("active");
		
		var c_region = $(this).attr("data-region");
		var c_zoom = region_zoom(c_region);
		var c_x = $(this).attr("data-map-x");
		var c_y = $(this).attr("data-map-y");
		var c_name = $(this).attr("data-title");
		
		$('#region-'+c_region).addClass("active");
					
		google.maps.event.addDomListener(window, 'load', init_map(c_x,c_y,c_name,c_zoom));
	});

	// all news page top slider
    $('#all_new_top_slider').lightSlider({
        item:1,
        loop:false,
        slideMove:1,
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed:600,
        responsive : [
            {
                breakpoint:480,
                settings: {
                    controls: false
                }
            }
        ]
    });

    // all news page tabs with news preview
    $(function () {
        var tabContainers = $('div.tabs > div'); // получаем массив контейнеров
        tabContainers.hide().filter(':first').show(); // прячем все, кроме первого
        // далее обрабатывается клик по вкладке
        $('div.tabs ul.tabNavigation a').click(function () {
            tabContainers.hide(); // прячем все табы
            tabContainers.filter(this.hash).show(); // показываем содержимое текущего
            $('div.tabs ul.tabNavigation a').removeClass('selected'); // у всех убираем класс 'selected'
            $(this).addClass('selected'); // текушей вкладке добавляем класс 'selected'
            return false;
        }).filter(':first').click();
    });

    // all news page pagination jQuery Pagination plugin
    var all_news_tabs_id_array = [];

    $('.tabs .all_news').each(function (index, item) {
		var id = $(item).children('ul').attr('id');
        all_news_tabs_id_array.push(id);

    });
	
    $(all_news_tabs_id_array).each(function (index, item) {

        var all_articles = $('#'+item).parent().find('article');
        var all_news_articles_length = $(all_articles).length;
        var allNewsPagesSize = Math.ceil(all_news_articles_length / 9);

		$('#'+ item).twbsPagination({
            totalPages: allNewsPagesSize,
            visiblePages: allNewsPagesSize > 5 ? 5 : allNewsPagesSize,
            next: '',
            prev: '',
            last: '...{{total_pages}} pages',
            first: '',
            onPageClick: function (event, page) {
                //fetch content and render here
                //$('#page-content').text('Page ' + page) + ' content here';

                /*
                    for now we use case when we get all news from the server, and render them to the page,
                    but show only first 6 from the start, and by clicking on pagination button, we change
                    visible articles according a page number
                */
                var from = 9 * page - 9;
                var to = from + 9;

                $(all_articles).each(function(item) {
                    if ( item >= from && (item + 1) <= to) {
                        $(all_articles[item]).addClass('visible');
                    }
                    else {
                        $(all_articles[item]).removeClass('visible');
                    }
                });

            }
        });
    });

    // make img full width on mobile screen in single news page
    function stretchImagesOnSingleNewsPage(array, boolean){
		if (boolean) {
            $(array).each(function(i, item){
                var image = $(item).find('img');
                $(image).addClass('mobile');
                var img_height = $(image).height() + 20;
                $(item).css('height', ''+img_height);
            })
		} else {
            $(array).each(function(i, item) {
                var image = $(item).find('img');
                $(image).removeClass('mobile');
                $(item).css('height', 'auto');
			});
		}
    };

	$(window).resize(function() {
        var singleNewsImages = $('.block_for_img');
        if (singleNewsImages.length > 0 && window.innerWidth <= 480) {
            stretchImagesOnSingleNewsPage(singleNewsImages, true);
        }else if (singleNewsImages.length > 0 && window.innerWidth > 480) {
            stretchImagesOnSingleNewsPage(singleNewsImages, false);
        }
	});

});




